package com.ponomarenko.hogwardslibrary.domain.repositories.spell

import com.ponomarenko.hogwardslibrary.data.network.RetrofitFactory
import com.ponomarenko.hogwardslibrary.domain.models.SpellModel
import com.ponomarenko.hogwardslibrary.domain.models.mapToDomain

class SpellRepositoryImpl: SpellRepository {
    override suspend fun getSpells(): List<SpellModel> {
        return RetrofitFactory.instance.spellService.getAllSpells(key = RetrofitFactory.key)
            .map { it.mapToDomain() }
    }
}