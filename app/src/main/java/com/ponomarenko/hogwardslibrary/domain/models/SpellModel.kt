package com.ponomarenko.hogwardslibrary.domain.models

import com.ponomarenko.hogwardslibrary.data.models.SpellRemote

data class SpellModel(val id: String, val name: String, val type: String)

fun SpellRemote.mapToDomain(): SpellModel {
    return SpellModel(
        id = this._id,
        name = this.spell,
        type = this.type)
}