package com.ponomarenko.hogwardslibrary.domain.models

data class FacultyModel(val name: String) {
}

fun String.mapToFacultyModel(): FacultyModel {
    return FacultyModel(
        name = this
    )
}