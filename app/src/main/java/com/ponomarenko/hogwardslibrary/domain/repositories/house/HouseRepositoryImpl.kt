package com.ponomarenko.hogwardslibrary.domain.repositories.house

import com.ponomarenko.hogwardslibrary.data.models.mapToModel
import com.ponomarenko.hogwardslibrary.data.network.RetrofitFactory
import com.ponomarenko.hogwardslibrary.data.services.HouseService
import com.ponomarenko.hogwardslibrary.domain.models.HouseModel
import com.ponomarenko.hogwardslibrary.ui.scenes.house.Houses
import java.lang.Exception

class HouseRepositoryImpl: HouseRepository {
    override suspend fun getHouseDetail(house: Houses): HouseModel? {
        val houseId = when(house) {
            Houses.Slitherin -> HouseService.slytherin
            Houses.Griffindor -> HouseService.gryffindorId
            Houses.Hufflepuff -> HouseService.hufflepuff
            Houses.Ravenclaw -> HouseService.ravenclawId
        }

        return try {
            RetrofitFactory
                .instance
                .houseService
                .getHouseDetail(key = RetrofitFactory.key, houseId = houseId)
                .filter { it._id == houseId }
                .map { it.mapToModel() }[0]
        } catch (e: Exception){
            return null
        }

    }
}