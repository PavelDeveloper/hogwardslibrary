package com.ponomarenko.hogwardslibrary.domain.repositories.spell

import com.ponomarenko.hogwardslibrary.domain.models.SpellModel

interface SpellRepository {
    suspend fun getSpells(): List<SpellModel>
}