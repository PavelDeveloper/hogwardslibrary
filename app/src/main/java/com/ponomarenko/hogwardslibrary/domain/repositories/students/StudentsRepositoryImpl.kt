package com.ponomarenko.hogwardslibrary.domain.repositories.students

import com.ponomarenko.hogwardslibrary.data.network.RetrofitFactory
import com.ponomarenko.hogwardslibrary.domain.models.StudentModel
import com.ponomarenko.hogwardslibrary.domain.models.mapToDomain

class StudentsRepositoryImpl: StudentsRepository {

    override suspend fun fetchStudents(): List<StudentModel> {
       return RetrofitFactory.instance.characterService.getAllCharacters(key = RetrofitFactory.key)
           .filter { it.role == "student" }
           .filter { it.house.isNotEmpty() }
           .map { it.mapToDomain() }
    }
}