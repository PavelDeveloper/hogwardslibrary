package com.ponomarenko.hogwardslibrary.domain.models

import com.ponomarenko.hogwardslibrary.data.models.HouseRemote

data class HouseModel(
    val name: String,
    val leader: String,
    val founder: String,
    val ghost: String
)