package com.ponomarenko.hogwardslibrary.domain.repositories.hat

import com.ponomarenko.hogwardslibrary.domain.models.FacultyModel

interface HatRepository {

    suspend fun generateFaculty(userName: String): FacultyModel

}