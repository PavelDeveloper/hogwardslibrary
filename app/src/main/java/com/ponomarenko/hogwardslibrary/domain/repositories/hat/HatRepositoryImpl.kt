package com.ponomarenko.hogwardslibrary.domain.repositories.hat

import com.ponomarenko.hogwardslibrary.data.network.RetrofitFactory
import com.ponomarenko.hogwardslibrary.domain.models.FacultyModel
import com.ponomarenko.hogwardslibrary.domain.models.mapToFacultyModel
import com.ponomarenko.hogwardslibrary.domain.repositories.hat.HatRepository
import kotlinx.coroutines.delay

class HatRepositoryImpl:
    HatRepository {
    override suspend fun generateFaculty(userName: String): FacultyModel {
        return RetrofitFactory.instance.houseService.sortingHat().mapToFacultyModel()
    }
}