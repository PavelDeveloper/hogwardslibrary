package com.ponomarenko.hogwardslibrary.domain.repositories.students

import com.ponomarenko.hogwardslibrary.domain.models.StudentModel

interface StudentsRepository {
    suspend fun fetchStudents(): List<StudentModel>
}