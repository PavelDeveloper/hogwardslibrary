package com.ponomarenko.hogwardslibrary.domain.repositories.house

import com.ponomarenko.hogwardslibrary.domain.models.HouseModel
import com.ponomarenko.hogwardslibrary.ui.scenes.house.Houses

interface HouseRepository {

    suspend fun getHouseDetail(houses: Houses): HouseModel?
}