package com.ponomarenko.hogwardslibrary.helpers

enum class Keys(val value: String) {
    Username("username"), Faculty("faculty")
}