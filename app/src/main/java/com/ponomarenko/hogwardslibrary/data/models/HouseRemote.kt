package com.ponomarenko.hogwardslibrary.data.models

import com.ponomarenko.hogwardslibrary.domain.models.HouseModel
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HouseRemote(
    val _id: String = "",
    val name: String = "",
    val mascot: String = "",
    val headOfHouse: String = "",
    val houseGhost: String = "",
    val founder: String = "",
    @SerialName("__v") val version: Int,
    val school: String = "",
    val members: List<Members>,
    val values: List<String>,
    val colors: List<String>
)

@Serializable
data class Members(val _id: String, val name: String)

fun HouseRemote.mapToModel(): HouseModel {
    return HouseModel(
        name = this.name,
        leader = this.headOfHouse,
        founder = this.founder,
        ghost = this.houseGhost
    )
}