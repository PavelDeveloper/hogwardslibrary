package com.ponomarenko.hogwardslibrary.data.services

import com.ponomarenko.hogwardslibrary.data.models.SpellRemote
import retrofit2.http.GET
import retrofit2.http.Query

interface SpellService {
    @GET("spells")
    suspend fun getAllSpells(@Query(value = "key") key: String): List<SpellRemote>
}