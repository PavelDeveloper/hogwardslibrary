package com.ponomarenko.hogwardslibrary.data.services

import com.ponomarenko.hogwardslibrary.data.models.HouseRemote
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface HouseService {

    companion object {
        val gryffindorId = "5a05e2b252f721a3cf2ea33f"
        val ravenclawId = "5a05da69d45bd0a11bd5e06f"
        val slytherin = "5a05dc8cd45bd0a11bd5e071"
        val hufflepuff = "5a05dc58d45bd0a11bd5e070"
    }

    @GET("houses/{houseId}")
    suspend fun getHouseDetail(
        @Path(value = "houseId") houseId: String,
        @Query(value = "key") key: String
    ): List<HouseRemote>

    @GET("./sortingHat")
    suspend fun sortingHat(): String
}