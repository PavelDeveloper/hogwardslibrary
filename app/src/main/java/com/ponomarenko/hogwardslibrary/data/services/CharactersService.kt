package com.ponomarenko.hogwardslibrary.data.services

import com.ponomarenko.hogwardslibrary.data.models.CharacterRemote
import retrofit2.http.GET
import retrofit2.http.Query

interface CharactersService {
    @GET("./characters")
    suspend fun getAllCharacters(@Query(value = "key") key: String): List<CharacterRemote>
}