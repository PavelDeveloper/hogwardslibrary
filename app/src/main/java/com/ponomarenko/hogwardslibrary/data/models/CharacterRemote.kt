package com.ponomarenko.hogwardslibrary.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CharacterRemote(
    val _id: String,
    val name: String = "",
    val role: String = "",
    val school: String = "",
    val alias: String = "",
    @SerialName("__v") val version: Int,
    val ministryOfMagic: Boolean,
    val house: String = "",
    val boggart: String = "",
    val patronus: String = "",
    val animagus: String = "",
    val wand: String = "",
    val orderOfThePhoenix: Boolean,
    val dumbledoresArmy: Boolean,
    val deathEater: Boolean,
    val bloodStatus: String = "",
    val species: String = ""
)