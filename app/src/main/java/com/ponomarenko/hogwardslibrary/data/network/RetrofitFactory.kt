package com.ponomarenko.hogwardslibrary.data.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.ponomarenko.hogwardslibrary.data.services.CharactersService
import com.ponomarenko.hogwardslibrary.data.services.HouseService
import com.ponomarenko.hogwardslibrary.data.services.SpellService
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import okhttp3.MediaType.Companion.toMediaType

class RetrofitFactory {

    companion object {
        const val key = "\$2a\$10\$9GJmEyG2nAyb6xLuAG6aBuMYq97AJQnFd8kTz4nFqEmVmv2jh.oZe"
        val instance = RetrofitFactory()
    }

    private fun okHttpInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(okHttpInterceptor())
        .build()

    private val retrofitClient:  Retrofit = Retrofit.Builder()
        .baseUrl("https://www.potterapi.com/v1/")
        .client(okHttpClient)
        .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
        .build()

    val characterService: CharactersService = retrofitClient.create(CharactersService::class.java)
    val houseService: HouseService = retrofitClient.create(HouseService::class.java)
    val spellService: SpellService = retrofitClient.create(SpellService::class.java)
}