package com.ponomarenko.hogwardslibrary.ui.scenes.hat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ponomarenko.hogwardslibrary.domain.repositories.hat.HatRepository
import com.ponomarenko.hogwardslibrary.domain.repositories.hat.HatRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HatViewModel: ViewModel() {

    private var hatRepository: HatRepository = HatRepositoryImpl()

    private val _userName: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
    private val _facultyName: MutableLiveData<String> = MutableLiveData<String>().apply{value = ""}

    val facultyName: LiveData<String> = _facultyName
    val isLoading: LiveData<Boolean> = _isLoading

    fun applyUserName(name: String) {
        _userName.postValue(name)
    }

    fun getFacultyName() {
        viewModelScope.launch {
            _isLoading.postValue(true)
            withContext(Dispatchers.IO) {
                _facultyName.postValue(hatRepository.generateFaculty(userName = _userName.value ?: "").name)
                _isLoading.postValue(false)
            }
        }
    }

}