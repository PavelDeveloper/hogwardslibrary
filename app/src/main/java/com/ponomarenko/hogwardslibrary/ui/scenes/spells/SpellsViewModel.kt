package com.ponomarenko.hogwardslibrary.ui.scenes.spells

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ponomarenko.hogwardslibrary.domain.repositories.spell.SpellRepositoryImpl
import com.ponomarenko.hogwardslibrary.ui.scenes.spells.adapters.SpellCellModel
import com.ponomarenko.hogwardslibrary.ui.scenes.spells.adapters.mapToUI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SpellsViewModel : ViewModel() {

    private val _filters = MutableLiveData<MutableList<String>>().apply {
        value = mutableListOf()
    }

    private val spellRepository = SpellRepositoryImpl()

    private val _spells = MutableLiveData<MutableList<SpellCellModel>>().apply {
        value = mutableListOf()
    }

    private val _spellsDisplay = MutableLiveData<MutableList<SpellCellModel>>().apply {
        value = ArrayList()
    }
    val spellsDisplay: LiveData<MutableList<SpellCellModel>> = _spellsDisplay

    init {
        fetchSpells()
    }

    private fun fetchSpells() {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                val spells = spellRepository.getSpells()
                _spells.postValue(spells.map { it.mapToUI() }.toMutableList())
                _spellsDisplay.postValue(_spells.value ?: ArrayList())
            }
        }
    }

    fun pressFilter(type: String, isSelected: Boolean) {
        if (isSelected) {
            _filters.value?.add(type)
        } else {
            _filters.value?.remove(type)
        }

        if (_filters.value?.isEmpty() == true) {
            _spellsDisplay.postValue(_spells.value ?: ArrayList())
            return
        }

        _spellsDisplay.postValue(_spells.value?.filter {
            _filters.value?.contains(it.type) ?: false
        }?.toMutableList())
    }
}