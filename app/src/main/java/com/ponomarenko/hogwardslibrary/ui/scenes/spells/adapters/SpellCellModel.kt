package com.ponomarenko.hogwardslibrary.ui.scenes.spells.adapters

import com.ponomarenko.hogwardslibrary.data.models.SpellRemote
import com.ponomarenko.hogwardslibrary.domain.models.SpellModel

data class SpellCellModel(val name: String, val type: String)

fun SpellModel.mapToUI(): SpellCellModel {
    return SpellCellModel(
        name = this.name,
        type = this.type)
}