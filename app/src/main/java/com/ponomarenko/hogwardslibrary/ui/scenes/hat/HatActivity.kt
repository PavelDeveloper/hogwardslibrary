package com.ponomarenko.hogwardslibrary.ui.scenes.hat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.ponomarenko.hogwardslibrary.R
import com.ponomarenko.hogwardslibrary.helpers.Keys
import com.ponomarenko.hogwardslibrary.ui.scenes.main.MainActivity
import kotlinx.android.synthetic.main.activity_hat.*
import java.security.Key

class HatActivity : AppCompatActivity() {

    lateinit var hatViewModel: HatViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hat)

        hatViewModel = ViewModelProviders.of(this).get(HatViewModel::class.java)

        textWelcomeUserName.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                hatViewModel.applyUserName(name = p0.toString())
            }

        })

        btnWelcomeSelect.setOnClickListener {
            if (btnWelcomeSelect.text == getString(R.string.welcome_next)) {
                val intent = Intent(applicationContext, MainActivity::class.java)
                intent.putExtra("faculty_name",  hatViewModel.facultyName.value)
                startActivity(intent)
                finish()
            } else {
                hatViewModel.getFacultyName()
            }
        }
        setupFaculty(viewModel = hatViewModel)
        setupLoading(viewModel = hatViewModel)

    }

    private fun setupFaculty(viewModel: HatViewModel) {
        viewModel.facultyName.observe(this, Observer {
            if (it.isNotEmpty()) {
                if (!TextUtils.isEmpty(textWelcomeUserName.text.toString())) {
                    val sharedPreferences = getSharedPreferences(getString(R.string.app_name), 0)
                    sharedPreferences.edit()
                        .putString(Keys.Username.value, textWelcomeUserName.text.toString())
                        .putString(Keys.Faculty.value, it)
                        .apply()
                }
                txtWelcomeSelected.text = getString(R.string.welcome_selected).replace("[faculty_name]", it)
                txtWelcomeSelected.visibility = View.VISIBLE
                btnWelcomeSelect.text = getString(R.string.welcome_next)
            }
        })
    }

    private fun setupLoading(viewModel: HatViewModel) {
        viewModel.isLoading.observe(this, Observer {
            textWelcomeUserName.isEnabled = !it
            btnWelcomeSelect.isEnabled = !it
            if (it)
                btnWelcomeSelect.text = getString(R.string.welcome_selecting)
        })
    }
}
