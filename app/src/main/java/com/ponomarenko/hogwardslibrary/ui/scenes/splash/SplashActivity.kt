package com.ponomarenko.hogwardslibrary.ui.scenes.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ponomarenko.hogwardslibrary.R
import com.ponomarenko.hogwardslibrary.helpers.Keys
import com.ponomarenko.hogwardslibrary.ui.scenes.hat.HatActivity
import com.ponomarenko.hogwardslibrary.ui.scenes.main.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val userName = getSharedPreferences(getString(R.string.app_name), 0)
            .getString(Keys.Username.value, "") ?: ""

        if (userName.isEmpty()) {
            startActivity(Intent(applicationContext, HatActivity::class.java))
        } else {
            startActivity(Intent(applicationContext, MainActivity::class.java))
        }
        finish()
    }
}
