package com.ponomarenko.hogwardslibrary.ui.scenes.house

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ponomarenko.hogwardslibrary.R
import com.ponomarenko.hogwardslibrary.domain.repositories.house.HouseRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HouseDetailViewModel : ViewModel() {

    val houseRepository = HouseRepositoryImpl()

    private val _ghost: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val _founder: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val _leader: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val _houseName: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    private val _houseImg: MutableLiveData<Int> =
        MutableLiveData<Int>().apply { value = R.drawable.img_gryffindor }

    val ghost: LiveData<String> = _ghost
    val founder: LiveData<String> = _founder
    val leader: LiveData<String> = _leader
    val houseName: LiveData<String> = _houseName
    val houseImg: LiveData<Int> = _houseImg

    fun fetchData(house: Houses?) {

        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                house?.let {
                    val houseDetail = houseRepository.getHouseDetail(house = house)
                    _ghost.postValue(houseDetail?.ghost.orEmpty())
                    _founder.postValue(houseDetail?.founder.orEmpty())
                    _leader.postValue(houseDetail?.leader.orEmpty())
                    _houseName.postValue(houseDetail?.name.orEmpty())
                    when (house) {
                        Houses.Griffindor -> _houseImg.postValue(R.drawable.img_gryffindor)
                        Houses.Slitherin -> _houseImg.postValue(R.drawable.img_slytherin)
                        Houses.Ravenclaw -> _houseImg.postValue(R.drawable.img_ravenclaw)
                        Houses.Hufflepuff -> _houseImg.postValue(R.drawable.img_hufflepuff)
                    }
                }
            }
        }
    }
}
