package com.ponomarenko.hogwardslibrary.ui.scenes.spells.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ponomarenko.hogwardslibrary.R
import kotlinx.android.synthetic.main.cell_spell.view.*

class SpellAdapter: RecyclerView.Adapter<SpellAdapter.SpellViewHolder>() {

    private val mDataList = ArrayList<SpellCellModel>()

    fun setData(newData: ArrayList<SpellCellModel>) {
        mDataList.clear()
        mDataList.addAll(newData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpellAdapter.SpellViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SpellViewHolder(inflater.inflate(R.layout.cell_spell, parent, false))
    }

    override fun getItemCount(): Int = mDataList.count()

    override fun onBindViewHolder(holderSpell: SpellAdapter.SpellViewHolder, position: Int) {
        holderSpell.bind(model = mDataList[position])
    }

    class SpellViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val txtSpellName: TextView = itemView.txtSpellName
        private val txtSpellType: TextView = itemView.txtSpellType

        fun bind(model: SpellCellModel) {
            txtSpellName.text = model.name
            txtSpellType.text = model.type
        }
    }
}