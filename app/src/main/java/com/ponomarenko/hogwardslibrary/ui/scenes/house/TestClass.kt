package com.ponomarenko.hogwardslibrary.ui.scenes.house

 class TestClass {
    fun play() {
    }

     fun fly() {

     }
}

fun main() {

    val testClass2 = TestClass()
    with(testClass2) {
        this.fly()
        play()
    }
//---------------------------------------------
    val testClass = TestClass().apply {
        play()
        fly()
    }.play()
//----------------------------------------------
    val testClass3: TestClass? = TestClass()

    testClass3?.let {
        it.play()
        it.fly()
    }
//----------------------------------------------

    testClass3?.also {
        it.fly()
    }?.play()

    var x = 3
    var y = 4

    y = x.also { x = y }

//----------------------------------------------


    testClass3?.run {
        play()
        fly()
    }

    /*вызовы функций объекта-получателя напрямую. Но run, в отличии от apply,
    возвращает результат работы лямбды. Интересная возможность этой функции — потоковый вызов ссылок на функции.*/

    fun checkServerResponse(code:Int):Boolean = code == 200
    fun serverResponseShowMessage(codeIsValid:Boolean) = if (codeIsValid){
        "с сервером всё в порядке"
    } else {
        "с сервером есть проблемы, выходные в опасности"
    }
    fun printMessage(message:String) = println(message)

    200
        .run(::checkServerResponse)
        .run(::serverResponseShowMessage)
        .run(::printMessage)

    // в java: printMessage(serverResponseShowMessage(checkServerResponse(200)))

}