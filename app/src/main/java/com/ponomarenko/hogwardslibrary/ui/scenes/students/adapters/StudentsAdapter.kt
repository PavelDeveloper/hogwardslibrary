package com.ponomarenko.hogwardslibrary.ui.scenes.students.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ponomarenko.hogwardslibrary.R
import kotlinx.android.synthetic.main.cell_student.view.*

class StudentsAdapter: RecyclerView.Adapter<StudentsAdapter.StudentViewHolder>() {

    private val mDataList = ArrayList<StudentCellModel>()
    private val mDisplayList = ArrayList<StudentCellModel>()

    fun setData(newData: List<StudentCellModel>) {
        mDataList.clear()
        mDataList.addAll(newData)
        filter(query = "")
    }

    fun filter(query: String) {
        mDisplayList.clear()
        if (query.isEmpty()) {
            mDisplayList.addAll(mDataList)
        } else {
            mDisplayList.addAll(mDataList.filter {
                it.name.contains(query, true) ||
                        it.facultyName.contains(query, true)
            })
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
       val inflater = LayoutInflater.from(parent.context)
        return StudentViewHolder(inflater.inflate(R.layout.cell_student, parent, false))
    }

    override fun getItemCount(): Int = mDisplayList.count()

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.bind(model = mDisplayList[position])
    }

    class StudentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val txtName: TextView = itemView.txtStudentName
        private val txtFaculty: TextView = itemView.txtStudentFaculty
        private val txtSpecies: TextView = itemView.txtStudentSpecies

        fun bind(model: StudentCellModel) {
            txtName.text = model.name
            txtFaculty.text = model.facultyName
            txtSpecies.text = model.species
        }
    }
}