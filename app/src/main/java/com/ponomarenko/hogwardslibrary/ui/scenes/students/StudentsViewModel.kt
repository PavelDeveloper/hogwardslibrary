package com.ponomarenko.hogwardslibrary.ui.scenes.students

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ponomarenko.hogwardslibrary.domain.repositories.students.StudentsRepository
import com.ponomarenko.hogwardslibrary.domain.repositories.students.StudentsRepositoryImpl
import com.ponomarenko.hogwardslibrary.ui.scenes.students.adapters.StudentCellModel
import com.ponomarenko.hogwardslibrary.ui.scenes.students.adapters.mapToUI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class StudentsViewModel : ViewModel() {

    private val studentsRepository = StudentsRepositoryImpl()
    private val _students = MutableLiveData<List<StudentCellModel>>().apply {
        value = ArrayList()
    }
    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }

    val students: LiveData<List<StudentCellModel>> = _students
    val isLoading: LiveData<Boolean> = _isLoading

    fun fetchStudents() {
        viewModelScope.launch {
            _isLoading.postValue(true)
            withContext(Dispatchers.Default) {
                val students = studentsRepository.fetchStudents()
                _isLoading.postValue(false)
                _students.postValue(students.map {
                    it.mapToUI()
                })
            }
        }
    }
}