package com.ponomarenko.hogwardslibrary.ui.scenes.students

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ponomarenko.hogwardslibrary.R
import com.ponomarenko.hogwardslibrary.ui.scenes.students.adapters.StudentsAdapter
import kotlinx.android.synthetic.main.activity_hat.*
import kotlinx.android.synthetic.main.fragment_students.*

class StudentsFragment : Fragment() {

    private lateinit var studentsViewModel: StudentsViewModel
    private val mAdapter = StudentsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        studentsViewModel =
            ViewModelProviders.of(this).get(StudentsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_students, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        studentsViewModel.fetchStudents()
        setupData()
        setupLoading()

        context?.let {
            recyclerStudents.adapter = mAdapter
            recyclerStudents.layoutManager =
                LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false)
        }

        textStudentSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mAdapter.filter(query = p0.toString())
            }
        })
    }

    private fun setupData() {
        studentsViewModel.students.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                mAdapter.setData(newData = it)
            }
        })
    }

    private fun setupLoading() {
        studentsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                txtStudentsDownloading.visibility = View.VISIBLE
                recyclerStudents.visibility = View.GONE
                textStudentSearch.visibility = View.GONE
            } else {
                txtStudentsDownloading.visibility = View.GONE
                recyclerStudents.visibility = View.VISIBLE
                textStudentSearch.visibility = View.VISIBLE
            }
        })
    }
}
