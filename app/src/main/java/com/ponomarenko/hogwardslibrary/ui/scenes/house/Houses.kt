package com.ponomarenko.hogwardslibrary.ui.scenes.house

enum class Houses (val id: String) {
    Griffindor(id = "Griffindor"), Hufflepuff(id = "Hufflepuff"),
    Ravenclaw(id = "Ravenclaw"), Slitherin(id = "Slitherin")
}